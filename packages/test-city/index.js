import TestCity from './src/main';

/* istanbul ignore next */
TestCity.install = function(Vue) {
  Vue.component(TestCity.name, TestCity);
};

export default TestCity;

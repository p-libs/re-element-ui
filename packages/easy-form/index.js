import EasyForm from './src/main';

/* istanbul ignore next */
EasyForm.install = function(Vue) {
  Vue.component(EasyForm.name, EasyForm);
};

export default EasyForm;

const fs = require('fs');
const path = require('path');

const editPath = path.resolve(__dirname, './theme-chalk');

const filenames = fs.readdirSync(editPath);
const matchFiles = filenames.filter((item) => {
  const isFile = fs.statSync(path.join(editPath, item)).isFile();
  const matchExt = path.extname(item) === '.css';

  return isFile && matchExt;
});
matchFiles.forEach((item) => {
  const source = path.join(editPath, item);
  const target = path.join(editPath, item.replace('css', 'scss'));

  fs.renameSync(source, target);
});

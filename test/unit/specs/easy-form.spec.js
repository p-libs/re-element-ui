import { createTest, destroyVM } from '../util';
import EasyForm from 'packages/easy-form';

describe('EasyForm', () => {
  let vm;
  afterEach(() => {
    destroyVM(vm);
  });

  it('create', () => {
    vm = createTest(EasyForm, true);
    expect(vm.$el).to.exist;
  });
});


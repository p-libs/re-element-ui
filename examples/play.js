import Vue from 'vue';
import Element from 'main/index.js';
import App from './play/index.vue';
// import 'packages/theme-chalk/src/index.scss';
import 'packages/theme-chalk/index.scss';
import 'packages/theme-sp/src/index.scss';

Vue.use(Element);

new Vue({
  // eslint-disable-line
  render: (h) => h(App)
}).$mount('#app');
